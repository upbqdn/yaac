\section{\uppercase{Introduction}}\label{sec:introduction}

The original name of the cipher for the Advanced Encryption Standard (AES) is
Rijndael, based on the names of two cryptographers---Joan Daemen and Vincent
Rijmen---who originally designed the cipher. In 1997, the U.S. National
Institute of Standards and Technology (NIST) announced the development of AES
and subsequently organized an open competition, which the Rijndael cipher won.
NIST published the cipher as the Federal Information Processing Standard (FIPS)
197~\cite{fips} in 2001.

% History often gives us valuable lessons of how things might end up. For
% example, one of the very first cryptographers, Gaius Julius Caesar, was
% attacked by his peers and stabbed to death. Alan Mathison Turing, a~successful
% cryptanalyst of the Enigma cipher, died of cyanide poisoning caused by his own
% hand. The study of cryptology is up to us now.

Algebraic cryptanalysis (AC) is an area of cryptanalysis that has gained much
attention in recent years~\cite{bard2009algebraic}. The principle of AC consists
in transferring the problem of breaking the cryptosystem to the problem of
solving a system of multivariate polynomial equations over a finite field that
belongs to the set of NP-complete problems. The process of AC is divided into
the following two steps. The first step consists of using the cipher's structure
and supplemental information to create a system of equations that describe the
behavior of the cipher for a specific case. Several papers
\cite{cid05},~\cite{simmons2009algebraic} present approaches for constructing
polynomial equations with auxiliary variables for AES. The paper
\cite{bulygin2010obtaining} presents a method for obtaining equations in key
variables only, which is based on Gröbner bases. In
Section~\ref{subsec:key_eqs}, we present another approach for obtaining
polynomial equations that contain only the variables of the initial key, which
is based on gradual substitution.

% AC is mainly used in symmetric ciphers, for example AES
% \cite{bulygin2010obtaining}, DES \cite{courtois2007algebraic} or Bluetooth
% stream cipher E0 \cite{la2022algebraic}, however AC was used also in
% asymmetric cryptography.

The second step of AC involves solving the polynomial system to derive the
secret key. While the method for deriving the system of equations depends on the
cipher, the method for solving the system may be independent of the cipher. In
our work, we leverage the fact that the derived equation systems contain only
the variables of the initial key, and we present some reduction techniques for
reducing the computational complexity of solving the polynomial systems.

Several previous studies have dealt with algebraic cryptanalysis of small scale
variants of AES\@. In~\cite{courtois2002cryptanalysis}, the authors described
AES as a system of overdefined sparse quadratic equations over GF(2), and
proposed an XSL attack for the family of XSL-ciphers to which AES belongs to.
The XSL algorithm was later analyzed concerning AES in~\cite{cid2005analysis}.
The work~\cite{bulygin2010obtaining} also presented methods for solving
polynomial systems derived from AES using Gröbner bases. The interpretation of
AES as a system of equations over GF($2^8$) is presented
in~\cite{murphy2002essential}. The work~\cite{nover2005algebraic} reviewed
different techniques for solving systems of multivariate quadratic equations
over arbitrary fields, such as relinearization and XL algorithm, that were used
on equations derived from AES\@.

We begin our work by a brief discussion of Gröbner bases, and we show how these
can be used to solve systems of multivariate non-linear polynomial equations. In
the third section, we derive multivariate non-linear polynomial systems over
GF(2) for small scale variants of AES\@. We will eliminate all auxiliary
variables by a gradual substitution so that the polynomial systems will contain
only the variables describing the secret key. The elimination will make the
polynomial systems fully dependent on the provided pairs of plaintext and
ciphertext, which will allow us to apply the reductions for faster solving.

The fourth section discusses the results of our experiments. We demonstrate the
current capabilities of Gröbner bases in solving the polynomial systems
described in the third section, and we compare their performance to a SAT
solver. We show how the performance of Gröbner bases and the SAT solver can be
increased using several pairs of plaintexts and their corresponding ciphertexts.
We also discuss the progress of diffusion within the reduced versions of AES\@.
In summary, our main contributions are: (1) the derivation of the equations
described in Section 3.2.4; (2) the processing of equations (see Section 4.1) to
speed up the computing of Gröbner bases.
