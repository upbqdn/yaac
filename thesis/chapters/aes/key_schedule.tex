\section{The Key Schedule}
\label{sec:key}

The \verb=AddRoundKey= transformation takes in four words (16 bytes) of the
expanded key \verb=expKey= and adds them to the state. Each word (4 bytes) is
added into a column of the state according to the following formula
\begin{equation}
  \label{eq:addRoundKey}
  \left( s'_{0, c}, s'_{1,c}, s'_{2, c}, s'_{3, c} \right)^T = \left( s_{0, c}, s_{1,c},
    s_{2, c}, s_{3, c} \right)^T \oplus \left( \verb=expKey=_{4 \cdot \text{round} +
      c} \right)^T
\end{equation}
for $0 \le c < 4$. The value of $round$ is 0 during the initial addition and 10
during the last addition. Otherwise, the value is defined by the loop in
Algorithm \ref{alg:aes}.

The values of the expanded key are defined by Algorithm \ref{alg:aes-key}. We
can see that the initial key is copied into the first four words of the
resulting \verb=expKey=. Each following word is the sum of the previous word and
the word four positions earlier. For words that are in positions that are
multiples of four, a~transformation is applied to the previous word before the
sum. This transformation is defined on line 11 of Algorithm \ref{alg:aes-key}
and described in the following paragraph.

The \verb=RotWord= operation takes a four-byte word $\left( x_0, x_1, x_2,
  x_3\right)^T$, where $x_i$ are individual bytes, and produces a cyclically
rotated word $\left( x_1, x_2, x_3, x_0 \right)^T$. The \verb=SubWord= operation
takes a four-byte word and applies the S-box to each byte in the word. The round
constant array \verb=Rcon[j]= contains ten four-byte words defined by $\left(
  02_{16}^{j - 1}, 00_{16}, 00_{16}, 00_{16} \right)^T$, where the only
effective part is the first byte with $02_{16}^{j - 1} \in \text{GF}(2^8)$ (or
$x^{j - 1} \in \text{GF}(2^8)$) being the powers of $02_{16} \in \text{GF}(2^8)$
(or $x \in \text{GF}(2^8)$).

\begin{algorithm}
  \caption{Key Schedule for AES}\label{alg:aes-key}

  \begin{tabular}{ l l}
    \textbf{Input:} & $\bm{\mathit{key}}$ --- array of 16 bytes \\
    \textbf{Output:} & key schedule $\bm{\mathit{expKey}}$ --- array of 44 words (176 bytes) \\
  \end{tabular}
  \\
  \begin{algorithmic}[1]
    \Function{ExpandKey}{$\mathit{key}$}
    \State word $\mathit{expKey}[44]$

    \For{$i \gets 0$ to 3}
    \State $\mathit{expKey}[i] \gets$
    word($\mathit{key}[4 \cdot i]$, $\mathit{key}[4 \cdot i + 1]$, $\mathit{key}[4 \cdot i + 2]$,
    $\mathit{key}[4 \cdot i + 3]$)
    \EndFor

    \For{$i \gets 4$ to 43}
    \State $\mathit{tmp} \gets \mathit{expKey}[i - 1]$

    \If{$i \equiv 0 \pmod{4}$}
    \State $\mathit{tmp} \gets$ SubWord(RotWord($\mathit{tmp}$)) $\oplus\ \mathit{Rcon}[i / 4]$
    \EndIf

    \State $\mathit{expKey} \gets \mathit{expKey}[i - 4] \oplus \textit{tmp} $
    \EndFor

    \State \Return $\mathit{expKey}$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

Figure \ref{fig:keyschedule} depicts four iterations of the loop defined on line
7 of Algorithm \ref{alg:aes-key}. Each vertical line in the figure represents a
4-byte word in the \verb=expKey= array. The box containing the transformation
$F_i$ is defined on line 11 of Algorithm \ref{alg:aes-key}, which is described in the
previous paragraph.

\begin{figure}[h]
  \centering \includegraphics[width=.5\textwidth]{keyschedule}
  \cprotect\caption{A~schematic depiction of the key schedule \cite{cid05}.}
  \label{fig:keyschedule}
\end{figure}