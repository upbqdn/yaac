\section{Fields, Polynomial Rings, Ideals and Varietes}

Let us introduce the rudiments of abstract algebra that will allow us to
progress towards the application of Gröbner bases in algebraic cryptanalysis and
towards the algebraic description of AES.

\begin{df} Let $A_1, \ldots, A_n$ be sets. Then the \textbf{Cartesian product} $A_1 \times
  \cdots \times A_n$ is the set of all ordered $n$-tuples $\left( a_1, \ldots, a_n
  \right)$ such that $a_i \in A_i$ for $1 \leq i \leq n$.
\end{df}

\begin{df} Let $A$ and $B$ be sets. A \textbf{map} is a set $\varphi \subseteq A \times B$ such
  that for each $a \in A$ there is exactly one $b \in B$ with $\left( a, b \right) \in
  \varphi$.
\end{df}

\begin{df} Let $A$ be a set. A \textbf{binary operation} is a map from $A \times A$
  to $A$.
\end{df}

Let us use the definition of a group in order to define the structures we are
going to operate with throughout the rest of our work --- rings, ideals, and
fields. Such an approach should make the definitions of these structures shorter
and emphasize their relations.

We first start with the definition of a simpler structure than a group:

\begin{df} A \textbf{monoid} is a set $M$ with a binary operation $\left( a, b
  \right) \mapsto a \circ b$ such that the following two axioms hold:
  \begin{enumerate}[label=(\roman*)]
  \item $\left( a \circ b \right) \circ c = a \circ \left( b \circ c \right)$ for all $a, b, c \in
    M$,
  \item there is $e \in M$ such that $e \circ a = a \circ e = a$ for all $a \in M$.
  \end{enumerate}
  A monoid is called a \textbf{commutative monoid} if, in addition to (i) and
  (ii), the following axiom also holds:
  \begin{itemize}
  \item[(iii)] $a \circ b = b \circ a$ for all $a, b \in M$.
  \end{itemize}
\end{df}

Note that since $\circ$ is a binary operation, the resulting element, $a \circ b$ is
always in $M$ for all $a , b \in M$. We say that $M$ is closed under $\circ$ or that
$\circ$ is closed on $M$. Also note that the first axiom is the associative
property. The element $e$ is called the \textbf{identity element} or simply the
\textbf{identity}. For simplicity, we will refer to the set $M$ as the monoid
with the associated operation being implicit. We will also use this convention
for all the subsequent algebraic structures, even when there will be multiple
operations associated with the structure.

\begin{df} A \textbf{group} $G$ is a monoid in which for all $a \in G$, there is
  $b \in G$ with $a \circ b = b \circ a = e$. A group $G$ is an \textbf{Abelian group} if
  it is also a commutative monoid.
\end{df}

The element $b$ in the definition above is called the \textbf{inverse} of $a$.
Note that Abelian groups are commutative groups.

\begin{df}
  \label{ring_df}
  A \textbf{ring} is a set $R$ with two binary operations $\left( a, b \right) \mapsto
  a + b$ and $\left( a, b \right) \mapsto a \cdot b$, referred to as addition and
  multiplication, such that the following axioms hold:
  \begin{enumerate}[label=(\roman*)]
  \item the set $R$ is an Abelian group under addition with the \textbf{additive
      identity} $0$,
  \item the set $R$ is a monoid under multiplication with the
    \textbf{multiplicative identity} $1$,
  \item $a \cdot \left( b + c \right) = a \cdot b + a \cdot c$ and $\left( a + b \right) \cdot c
    = a \cdot c + b \cdot c$ for all $a, b, c \in R$.
  \end{enumerate} A ring is a \textbf{commutative ring} if, under
  multiplication, $R$ is a commutative monoid.
\end{df}

The inverse under addition in a ring is called the \textbf{additive inverse},
and the inverse under multiplication is the \textbf{multiplicative inverse}.
Note that the axiom (iii) describes the left and right distributive laws. We
will usually omit the symbol for multiplication, and instead of $a \cdot b$, we will
write $ab$. Also note that subtraction in a ring can be thought of as addition
of the additive inverse.

\begin{es}
  \begin{enumerate}[label=(\roman*)]
  \item The sets $\mathbb{Z}$, $\mathbb{Q}$, $\mathbb{R}$, and $\mathbb{C}$ are
    rings with their standard addition and multiplication.
  \item The natural numbers do not form a ring since not all elements have their
    additive inverse in this set.
  \item The set of integers modulo $n \in \mathbb{Z}$, denoted $\mathbb{Z}_n$, is
    a ring.
  \end{enumerate}
\end{es}

\begin{df}
  \label{ideal_df}
  Let $R$ be a commutative ring and $\emptyset \ne I \subseteq R$. Then $I$ is an \textbf{ideal}
  of $R$ if:
  \begin{enumerate}[label=(\roman*)]
  \item $a+b \in I$ for all $a, b \in I$, and
  \item $ar \in I$ for all $a \in I$ and $r \in R$.
  \end{enumerate} The ideal $I$ is \textbf{proper} if $I \ne R$.
\end{df}

Note that an ideal $I$ of a ring $R$ is closed under addition. It is also closed
under multiplication by any $r \in R$.

\begin{pr}
  \label{pr:ideal_properties}
  Let $I$ be an ideal of a commutative ring $R$, then:
  \begin{enumerate}[label=\normalfont{(\roman*)}]
  \item $a \cdot 0 = 0 \cdot a = 0$ for all $a \in R$.
  \item $0 \in I$, and
  \item if $1 \in I$ then $I$ is not proper.
  \end{enumerate}
\end{pr}

\begin{samepage}
  \begin{p}
    ~\begin{enumerate}[label=(\roman*)]
    \item Suppose $a \in R$. Then
      \begin{eqnarray*} a + a \cdot 0 & = & a \cdot 1 + a \cdot 0 \\
                                  & = & a \left( 1 + 0 \right) \\
                                  & = & a \cdot 1 \\
                                  & = & a.
      \end{eqnarray*}
      Adding the additive inverse of $a$ on both sides gives $a \cdot 0 = 0$. Since
      $R$ is commutative, $0 \cdot a = 0$ also holds.
    \item Considering the previous proof, by (i) of Definition \ref{ring_df}, we
      know that $0 \in R$ and by (ii) of Definition \ref{ideal_df}, we get $0 \cdot a =
      0 \in I$ for any $a \in I$.
    \item Since $1$ is the multiplicative identity, we have $1 \cdot r = r \in I$ for
      all $r \in R$ and thus $I = R$. \qedhere
    \end{enumerate}
  \end{p}
\end{samepage}

\begin{re}
  \label{ideal_re}
  There is an analogy from modular arithmetic that illustrates an intuitive view
  of ideals --- they can be regarded as a generalization of a zero in a number
  set such as the integers. Consider the ring $\mathbb{Z}_n$ of integers modulo
  a given integer $n \in \mathbb{Z}$. The exact set of integers that we identify
  with $0$ in $\mathbb{Z}_n$ is the set $n \mathbb{Z} = \set*{nm\ |\ m \in
    \mathbb{Z}}$. This set meets the criteria for being an ideal ((i) and (ii)
  of Definition \ref{ideal_df}) of $\mathbb{Z}$ and its elements ``behave'' like
  $0$ in $\mathbb{Z}$: adding two elements of $n \mathbb{Z}$ yields another
  element of $n \mathbb{Z}$ and multiplying any element of $n \mathbb{Z}$ again
  yields an element of $n \mathbb{Z}$.
\end{re}

Considering our definition of rings, note that an ideal might not be a ring
itself. For example, consider the ring of integers and its ideal consisting of
even numbers. This ideal is not a ring since it has no multiplicative identity.

\begin{df}
  Let $I$ be an ideal of a ring $R$. The \textbf{coset} of $I$ defined by $a \in
  R$ is the set $\set*{b + a\ |\ b \in I}$ and denoted $I + a$ or $[a]$.
\end{df}

Let us define the addition of cosets by $\left( I + a \right) + \left( I + a'
\right) = I + \left( a + a' \right)$ and multiplication by $\left( I + a
\right)\left( I + a' \right) = I + aa'$. It can be shown that the set of all
cosets forms a ring under these operations. The proof can be found in
\cite[Chapter~7.3]{shoup}. This ring is denoted $R / I$ and called the
\textbf{quotient ring}.

\begin{df}
  A \textbf{field} $\F$ is a ring where the set $\F \setminus \set*{0}$ is an
  Abelian group under multiplication with the \textbf{multiplicative identity}
  1.
\end{df}

Fields with a finite number of elements are \textbf{finite fields} and are often
denoted $\mathbb{F}_q$ or GF($q$) (in honor of Évariste Galois, 1811--1832), where the
number of elements $q$ is the \textbf{order} of the field. Since we will not
encounter any rings that are not commutative, we will adopt the convention that
by a ring, we will mean a commutative ring. Then, the only difference between
rings and fields is that in a field, every element other than $0$ has its
multiplicative inverse. Note that every field is a ring as well.

\begin{es}\label{e:ff}
  \begin{enumerate}[label=(\roman*)]
  \item The sets $\mathbb{Q}$, $\mathbb{R}$, and $\mathbb{C}$ are fields with
    their standard addition and multiplication.
  \item The integers do not form a field since not all elements have their
    multiplicative inverse in this set.
  \item The set of integers modulo $p \in \mathbb{Z}$, denoted $\mathbb{Z}_p$, is
    a field whenever $p$ is prime. The primality of $p$ ensures that each
    non-zero element has its multiplicative inverse. We can find the inverses by
    leveraging the extended Euclidean algorithm.
  \end{enumerate}
\end{es}

\begin{re}\label{re:ff}
  We will often work with the finite field $\mathbb{Z}_2$, which merits a short
  comment. We will denote this field $\mathbb{F}_2$ or GF(2). The additive and
  multiplicative identities are $0$ and $1$, respectively. The additive inverse
  of $0$ is $0$. The element $1$ is also its additive and multiplicative
  inverse. Note that addition in this field corresponds to the \emph{exclusive
    or} operation denoted XOR or $\oplus$. Also note that subtraction is
  identical to addition. Owing to these nice properties, we will model single
  bits (binary digits) as elements of $\mathbb{F}_2$.
\end{re}

\begin{df}
  If $\F$ is a subfield of a field $\E$, then we call $\E$ an \textbf{extension
    field} of $\F$.
\end{df}

For example, GF($2^2$) is an extension field of GF(2). We give a more detailed
way of constructing extension fields in the proof of
Proposition \ref{pr:extension_field}.

% \begin{df}
%   Let $F$ be a field. An $\mathbf{F}$\textbf{-vector space} $V$ is an Abelian
%   group with addition and an additional operation $\circ : F \times V \mapsto V$, called
%   \textbf{scalar multiplication}, such that for all $\alpha, \beta \in F$ and $v, w \in V$,
%   the following properties hold:
%   \begin{enumerate}[label=(\roman*)]
%   \item $\alpha \circ \left( v + w \right) = \alpha \circ v + \alpha \circ w$,
%   \item $\left( \alpha + \beta \right) \circ v = \alpha \circ v + \beta \circ v$,
%   \item $\left( \alpha \cdot \beta \right) \circ v = \alpha \circ \left( \beta \circ v \right)$, and
%   \item $1 \circ v = v$.
%   \end{enumerate}
% \end{df}

% We call the elements of the vector space $V$ \textbf{vectors}, whereas the
% elements of the field $F$ are called \textbf{scalars}. Even though the zero
% scalar and zero vector are different objects, we will denote them both by 0.

% \begin{df}
%   Let $v_1, \ldots, v_n$ be pairwise different vectors in an $F$-vector space. Then
%   the sum \[\sum_{i = 1}^{n} \alpha_i \cdot v_i \quad \left( \alpha_i \in F \text{ for } 1 \le i \le n
%     \right)\] is a \textbf{linear combination} of the $v_i$ with coefficients
%   $\alpha_i$.
% \end{df}

% \begin{e}
%   Let $F$ be a field and $1 \le n \in \mathbb{N}_{>0}$. Define an addition on
%   $F^n$ by setting \[\left( v_1, \ldots, v_n \right) + \left( w_1, \ldots, w_n \right) =
%     \left( v_1 + w_1, \ldots, v_n + w_n\right)\] where $v_i, w_i \in F$ for $1 \le i \le
%   n$.
% \end{e}


% \section{Polynomials and Varietes}

% Polynomials are the moving spirit behind algebra. In the next chapter, we will
% describe a way how to obtain the model AES as a system of multivariate
% polynomials. On the other hand, the design of AES itself is based on
% univariate polynomials.

% The design of AES is based on univariate polynomials; while on the other
% hand, we will model the cipher as a system of multivariate polynomials.

\begin{df}
  Let $\alpha = \left( \alpha_1, \ldots, \alpha_n \right) \in \N^n$ be an $n$-tuple of
  non-negative integers. A \textbf{monomial} in $x_1, \ldots, x_n$ is a product
  of the form \[\prod_{i=1}^n x_i^{\alpha_i} = x_1^{\alpha_1} \cdot
    x_2^{\alpha_2} \cdots x_n^{\alpha_n}.\]
\end{df}

Let us simplify the notation by setting \[x^\alpha = \prod_{i=1}^n x_i^{\alpha_i}.\] The
\textbf{total degree} or \textbf{degree} of a monomial $x^\alpha$ is the sum
$\sum_{i=1}^n \alpha_i.$ We simplify the notation again an let $\lvert x^\alpha \rvert$
denote the total degree of $x^\alpha$. We will call the symbols $x_1, \ldots, x_n$
variables. Note that $x^\alpha = 1$ when $\alpha = \left( 0, \ldots, 0 \right)$ and also when
$\lvert x^\alpha \rvert = 0$. Also note that any monomial is fully determined by $\alpha$.

\begin{df}
  Let $x^\alpha$ be a monomial and let $\F$ be a field. A \textbf{term} with a
  non-zero \textbf{coefficient} $c_\alpha \in \F$ is the product $c_\alpha
  x^\alpha$.
\end{df}

\begin{df}
  A \textbf{polynomial} $f$ with coefficients in a field $\F$ is a finite sum of
  terms in the form
  \[
    f = \sum_\alpha c_\alpha \cdot x^\alpha, \quad c_\alpha \in \F.
  \]
  The zero polynomial will be denoted $0$.
\end{df}

The standard addition and multiplication of polynomials can be defined in the
following way.

\begin{df}\label{df:poly_op}
  Let
  \[
    f = \sum_{\alpha \in \N^n} c_\alpha x^\alpha \quad \text{and} \quad g = \sum_{\alpha \in \N^n} d_\alpha x^\alpha
  \]
  be two polynomials.
  ~\begin{enumerate}[label=(\roman*)]
  \item Their \textbf{sum} is defined as
    \[
      f + g = \sum_{\alpha \in \N^n} (c_\alpha + d_\alpha) x^\alpha,
    \]
  \item and their \textbf{product} as
    \[
      f \cdot g = \sum_{\gamma \in \N^n} \left( \sum_{\alpha + \beta = \gamma} c_\alpha d_\beta \right) x^\gamma.
    \]
  \end{enumerate}
\end{df}

Note that addition consists of adding the coefficients of like powers of $x$.

The set of all polynomials in $x_1, \ldots, x_n$ with coefficients in a field $\F$
will be denoted $\F\!\left[ x_1, \ldots, x_n \right]$. When the particular variables
are of no relevance, we will denote the set by $\F\!\left[\mathbf{x} \right]$
for short. We will also employ the standard letters $x, y$ and $z$ instead of
$x_1, x_2$ and $ x_3$ when we discuss illustrative polynomials. Polynomials of
one variable, called univariate polynomials, will be denoted by $f\!\left( x
\right) \in \F\!\left[ x \right]$.

\begin{df}
  Let $f = \sum c_\alpha x^\alpha \ne 0 \in \F\!\left[\textbf{x} \right]$ be a
  non-zero polynomial. The \textbf{total degree} or \textbf{degree} of $f$,
  denoted deg$\left( f \right)$, is the maximum $\lvert x^\alpha \rvert$ such
  that the corresponding coefficient $c_\alpha$ is nonzero. The degree of $0$ is
  undefined.
\end{df}

Let $f,g \in \F\!\left[ \mathbf{x} \right]$ be polynomials. We say that $f$
\emph{divides} $g$ and write $f \mid g$ if $g = fh$ for some polynomial $h \in
\F\!\left[ \mathbf{x} \right]$. One can show that the set $\F\!\left[ \mathbf{x}
\right]$ satisfies all of the ring axioms under standard polynomial addition and
multiplication. We will therefore refer to $\F\!\left[ \mathbf{x} \right]$ as a
\emph{polynomial ring}. Not all polynomials in this ring have their
multiplicative inverses, e.g., even the elementary polynomial $x_1$ does not
have its multiplicative inverse and so $\F\!\left[ \mathbf{x} \right]$ does not
form a field. A proof that $\F\!\left[ \mathbf{x} \right]$ forms a ring can be
found in \cite[Chapher~2]{gb}, the authors also provide a broader outlook on
polynomials by defining them in a more abstract way.

\begin{df}\label{df:irr}
  Let $f\!\left( x \right) \in \F\!\left[ x \right]$ be a univariate polynomial of
  positive degree. The polynomial $f\!\left( x \right)$ is \textbf{irreducible}
  over $\F\!\left[ x \right]$ if there is no factorization of the form
  $f\!\left( x \right) = p\!\left( x \right)q\!\left( x \right)$, where
  $p\!\left( x \right)$ and $q\!\left( x \right)$ are also univariate
  polynomials of positive degree in $\F\!\left[ x \right]$.
\end{df}

\begin{df}
  Let $\set*{f_1, \ldots, f_s} \subset \F\!\left[ \mathbf{x} \right]$ be a set of
  polynomials. Then we set \[\langle f_1, \ldots, f_s \rangle =
    \set*{\sum_{i=1}^s h_if_i~\bigg|~h_1, \ldots, h_s \in \F\!\left[ \mathbf{x}
      \right]}.\]
\end{df}

\begin{lm}
  If $\set*{f_1, \ldots, f_s} \subset \F\!\left[ \mathbf{x} \right]$ is a set of
  polynomials, then $\langle f_1, \ldots, f_s \rangle$ is an ideal of $\F\!\left[
    \mathbf{x} \right]$.
\end{lm}

\begin{p}
  Assume $f = \sum_{i=1}^s p_if_i$ and $g = \sum_{i=1}^s q_if_i$ are polynomials, and
  let also $h \in \F\!\left[ \mathbf{x} \right]$ be a polynomial. Then the
  equations
  \begin{align*}
    f + g &= \sum_{i=1}^s \left( p_i + q_i \right) f_i \text{\quad and} \\
    hf &= \sum_{i=1}^s \left( hp_i \right) f_i
  \end{align*}
  show that $\langle f_1, \ldots, f_s \rangle$ meets the criteria for being an
  ideal of $\F\!\left[ \mathbf{x} \right]$.
\end{p}

\begin{df}
  \label{df:basis}
  Let $\set*{f_1, \ldots, f_s} \subset \F\!\left[ \mathbf{x} \right]$ be a set of
  polynomials and let $I$ be an ideal such that $I = \langle {f_1, \ldots, f_s}
  \rangle$. The set $\set*{f_1, \ldots, f_s}$ is a \textbf{basis} of $I$. We
  will also call $\langle {f_1, \ldots, f_s} \rangle$ the \textbf{ideal
    generated by} $\set*{f_1, \ldots, f_s}$.
\end{df}

Remark \ref{ideal_re} provides an intuitive view of ideals through modular
arithmetic. Another analogy comes from linear algebra where the definition of
subspaces can be likened to the definition of ideals of polynomial rings. Both
are closed under addition. Subspaces are closed under multiplication by scalars
while ideals of polynomial rings are closed under multiplication by polynomials.
An ideal generated by a set of polynomials also shares similar properties with a
span generated by a set of vectors, which is a structure similar to subspaces as
well.

\begin{df}
  \sloppy Let $\F$ be a field and $n$ a positive integer. The
  \mbox{$n$-dimensional} \textbf{affine space} over $\F$ is the set \[\F^n =
    \set*{\left( a_1, \ldots, a_n \right)\ |\ a_1, \ldots, a_n \in \F}.\]
\end{df}

\begin{re} \label{poly_re} A polynomial $f \in \F\!\left[ x_1, \ldots, x_n \right]$
  can be regarded as a function $f : \F^n \mapsto \F$ that takes in points in the
  affine space $\F^n$ and produces elements of the field $\F$.
\end{re}

\begin{df}
  Let $\F^n$ be an affine space and let $f = f\!\left( x_1, \ldots, x_n \right) \in
  \F\!\left[ x_1, \ldots, x_n \right]$ be a polynomial. The \textbf{zero point} or \textbf{root}
  of $f$ is a point $\mathbf{a} = \left( a_1, \ldots, a_n \right) \in \F^n$ such that
  $f\!\left( \mathbf{a} \right) = 0$.
\end{df}

\begin{df}
  Let $\set*{f_1, \ldots, f_s} \subset \F\!\left[ x_1, \ldots, x_n \right]$ be a set
  of polynomials and $\F^n$ an affine space. The \textbf{affine variety}
  $V\!\left( f_1, \ldots, f_s \right)$ defined by $\set*{f_1, \ldots, f_s}$ is
  the set \[V\!\left( f_1, \ldots, f_s \right) = \set*{\left( a_1, \ldots, a_n
      \right) \in \F^n~\Big|~f_i\!\left( a_1, \ldots, a_n \right) = 0 \text{ for
        all } 1 \le i \le s}\] of all zero points of all the polynomials in
  $\set*{f_1, \ldots, f_s}$.
\end{df}

Solving an equation that can be expressed as a polynomial in multiple variables
can be seen as finding the zero points of the corresponding polynomial. Affine
varieties generalize this notion to systems of polynomial equations. Considering
Remark \ref{poly_re}, we may also see varieties as geometric objects, which is
briefly illustrated by the following example:

\begin{e}
  Consider the real coordinate space $\mathbb{R}^2$ and the polynomial $f(x, y)
  = f\!\left( x^2 + y^2 - 1 \right)$. The variety $V\!\left( f \right)$ is the
  unit circle centered at the origin.
\end{e}

We will use the following lemma to show that a given ideal is contained in
another one. This is useful for proving the equality of two ideals in Example
\ref{ideal_e}.

\begin{lm}
  \label{ideal_lm}
  Let $I \subseteq \F\!\left[ \mathbf{x} \right]$ be an ideal, and let $\set*{f_1,
    \ldots, f_s} \subset \F\!\left[ \mathbf{x} \right]$ be a set of polynomials.
  Then $\langle f_1, \ldots, f_s \rangle \subseteq I$ if and only if $\set*{f_1,
    \ldots, f_s} \subseteq I$.
\end{lm}

\begin{ps}
  \begin{itemize}
  \item[$\implies$] Assume $\langle {f_1, \ldots, f_s} \rangle \subseteq I$. Each
    $f_i \in \set*{f_1, \ldots, f_s}$ can be constructed as follows: $f_i = 0
    \cdot f_1 + \cdots + 1 \cdot f_i + \cdots + 0 \cdot f_s$, and hence
    $\set*{f_1, \ldots, f_n} \subseteq I$.
  \item[$\impliedby$] Assume $\set*{f_1, \ldots, f_n} \subseteq I$ and choose any
    $f \in \langle {f_1, \ldots, f_s} \rangle$ so that $f = h_1f_1 + \cdots +
    h_sf_s$ where each $h_i \in \F\!\left[ \mathbf{x} \right]$. We see that $f
    \in I$ since $I$ is an ideal and so $\langle {f_1, \ldots, f_s} \rangle
    \subseteq I$. \qedhere
  \end{itemize}
\end{ps}

\begin{e}
  \label{ideal_e}
  Consider the ideals $\langle {x, y} \rangle$ and $\langle {x + y, x - y}
  \rangle$ in the polynomial ring $\mathbb{Q}\!\left[ x, y \right]$. We will
  show that these two ideals are equal so that $\langle {x, y} \rangle = \langle
  {x + y, x - y} \rangle$.

  We see that $x + y \in \langle {x, y} \rangle$ and $x - y \in \langle {x, y}
  \rangle$, so by Lemma \ref{ideal_lm}, $\langle {x + y, x - y} \rangle
  \subseteq \langle {x, y} \rangle$. Similarly, both $x = \frac{1}{2}\!\left( x
    + y \right) + \frac{1}{2}\!\left( x - y \right)$ and $y =
  \frac{1}{2}\!\left( x + y \right) - \frac{1}{2}\!\left( x - y \right)$ are in
  $\langle {x + y, x - y} \rangle$ so that by Lemma \ref{ideal_lm}, $\langle {x,
    y} \rangle \subseteq \langle {x + y, x - y} \rangle$ and the equality
  follows.
\end{e}

\begin{pr}
  \label{pr:ideal}
  If $\set*{f_1, \ldots, f_s}$ and $\set*{g_1, \ldots, g_t}$ are two bases of the
  same ideal in $\F\!\left[ x_1, \ldots, x_n \right]$, so that $\langle f_1,
  \ldots, f_s \rangle = \langle g_1, \ldots, g_t \rangle$, then $V\!\left( f_1,
    \ldots, f_s \right) = V\!\left(g_1, \ldots, g_t \right)$.
\end{pr}

\begin{p}
  \sloppy Choose any $\left( a_1, \ldots, a_n \right) \in V\!\left( f_1, \ldots, f_s
  \right)$. We know that all polynomials in $\set*{f_1, \ldots, f_s}$ are equal
  to zero at $\left( a_1, \ldots, a_n \right)$. Now choose any $g \in \langle
  {g_1, \ldots, g_t} \rangle$. Since $\langle {g_1, \ldots, g_t} \rangle =
  \langle {f_1, \ldots, f_s} \rangle$, we can write $g = \sum_{i=1}^s h_i f_i$,
  $h_i \in \F\!\left[x_1, \ldots, x_n \right]$. Then $g\!\left( a_1, \ldots, a_n
  \right) = \sum_{i=1}^s h_i\!\left( a_1, \ldots, a_n \right) \cdot f_i\!\left(
    a_1, \ldots, a_n \right) = 0,$ which shows that $\left( a_1, \ldots, a_n
  \right) \in V\!\left( g_1, \ldots, g_t \right)$, which means that $V\!\left(
    f_1, \ldots, f_s \right) \subseteq V\!\left( g_1, \ldots, g_t \right)$. The
  opposite inclusion can be proved in the same way.
\end{p}

The following theorem shows that every ideal can be generated by a finite basis.

\begin{thm}[Hilbert Basis Theorem]
  \label{thm:HLB}
  For every ideal $I \subseteq \Fg$ we have $I = \langle \s[m]{g} \rangle$ for
  some $\s[m]{g} \in I$.
\end{thm}

\begin{p}
  See \cite[p. 77]{iva}.
\end{p}

So far, we were thinking of varietes as the sets of solutions of finite sets of
polynomial equations. The Hilbert Basis Theorem shows that we can also think of
varietes defined by ideals.

\begin{df}
  Let $I \subseteq \Fx{x}$ be an ideal. We denote by $V(I)$ the set
  \[
    V(I) = \{\sn{a} \in \F^n \mid f\n{a} = 0 \text{ for all } f \in I\}.
  \]
\end{df}

\begin{pr}
  \label{pr:variety}
  \sloppy $V(I)$ is an affine variety. In particular, if $I = \langle \s[m]{f}
  \rangle$, then $V(I) = V\n[m]{f}$.
\end{pr}

\begin{p}
  See \cite[p. 81]{iva}.
\end{p}

Example \ref{ideal_e} shows that an ideal may have multiple different bases while
propositions \ref{pr:ideal} and \ref{pr:variety} reveal that a variety is
actually determined by the ideal generated by its basis and not by the basis
itself. In combination with the Hilbert Basis Theorem, Proposition
\ref{pr:variety} also shows that even though we have infinitely many polynomials
in a nonzero ideal, its variety $V(I)$ can still be defined by a finite set of
polynomial equations. Proposition \ref{pr:variety} is in fact a generalization of
Proposition \ref{pr:ideal}.

A system of multivariate equations can be seen as an ideal basis. Propositions
\ref{pr:ideal} and \ref{pr:variety} then give us a potential ability to change
the original system to another one while keeping the exact same solution set. We
will model our cipher as system of polynomial equations and then we will
transform this system into a new one which will be solvable in linear time. We
will show that a specific Gröbner basis can be the new system and that the
transformation will be the most demanding part of the computation as regards
both time and memory.

\begin{df}
  Let $V \subseteq \F^n$ be an affine variety. We define
  \[
    I(V) = \{ f \in \Fx{x} \mid f\n{a} = 0 \text{ for all } \sn{a} \in V \}.
  \]
\end{df}

\begin{pr}
  \label{pr:VIdeal}
  If $V \subseteq \F^n$ is an affine variety, then $I(V) \subseteq \Fx{x}$ is an
  ideal. We call $I(V)$ the \textbf{ideal of} $V$.
\end{pr}

\begin{p}
  We have $0 \in I(V)$ since the zero polynomial vanishes on all points in
  $\F^n$. Now let $f, g \in I(V), h \in \Fx{x}$ and let $\sn{a}$ be an arbitrary
  point in $V$. We get $f\n{a} + g\n{a} = 0 + 0 = 0$ and $h\n{a} f\n{a} =
  h\n{a} \cdot 0 = 0$, which shows that $I(V)$ is an ideal.
\end{p}

We will use Proposition \ref{pr:VIdeal} in order to combine and subsequently
reduce our polynomial systems during the experiments. As we will see, this will
allow us to compute the solutions of larger systems, which we would be not able
to obtain otherwise.